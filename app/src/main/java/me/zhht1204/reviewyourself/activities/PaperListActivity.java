package me.zhht1204.reviewyourself.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import me.zhht1204.reviewyourself.R;
import me.zhht1204.reviewyourself.utils.MyDbHelper;

public class PaperListActivity extends AppCompatActivity {
	private int paperId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_paper_list);
		final List<String> paperList = new ArrayList<String>();
		final MyDbHelper dbHelper = new MyDbHelper(getApplicationContext());
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query("papers", new String[]{"paper"}, "", null, "", "", "id desc");
		while (cursor.moveToNext()) {
			paperList.add(cursor.getString(0));
		}
		cursor.close();
		ListView listView = (ListView)findViewById(R.id.paperListView);
		ListAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paperList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				String paperName = paperList.get(i);
				String type = getIntent().getStringExtra("type");
				Intent intent = new Intent();
				intent.putExtra("paperName", paperName);
				if(type.equals("review")) {
					intent.setClass(getApplicationContext(), ReviewActivity.class);
				} else {
					intent.setClass(getApplicationContext(), QuestionListActivity.class);
				}
				startActivity(intent);
			}
		});

	}
}

package me.zhht1204.reviewyourself.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import me.zhht1204.reviewyourself.R;
import me.zhht1204.reviewyourself.objects.Question;
import me.zhht1204.reviewyourself.objects.QuestionTypes;
import me.zhht1204.reviewyourself.utils.MyDbHelper;

public class QuestionListActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_question_list);
		Intent intent = this.getIntent();
		String paperName = intent.getStringExtra("paperName");
		final List<String> questionList = new ArrayList<String>();
		final MyDbHelper dbHelper = new MyDbHelper(getApplicationContext());
		final SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query("questions", new String[]{"id", "context"}, "paper=?",
				new String[]{paperName}, "", "", "id desc");
		while (cursor.moveToNext()) {
			questionList.add(cursor.getInt(0) + ". " + cursor.getString(1));
		}

		final AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage("确认删除吗？");
		builder.setTitle("提示");
		ListView listView = (ListView) findViewById(R.id.questionListView);
		ListAdapter adapter =
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, questionList);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
				final String questionId = questionList.get(i).split("\\.", 2)[0];
				final int index = i;
				builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						db.execSQL("delete from questions where id = " + questionId);
						questionList.remove(index);
						Toast.makeText(getApplicationContext(), "删除成功", Toast.LENGTH_SHORT).show();
						dialog.dismiss();
					}
				});

				builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				builder.create().show();
			}
		});
		cursor.close();
	}
}

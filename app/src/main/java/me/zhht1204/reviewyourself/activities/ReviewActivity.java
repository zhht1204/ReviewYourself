package me.zhht1204.reviewyourself.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import me.zhht1204.reviewyourself.R;
import me.zhht1204.reviewyourself.objects.Question;
import me.zhht1204.reviewyourself.objects.QuestionTypes;
import me.zhht1204.reviewyourself.utils.MyDbHelper;

public class ReviewActivity extends AppCompatActivity {
	private int checkedId;
	private int answerViewId;
	private String type;
	private String rightAnswer;
	private List<String> answerList;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_review);
		Intent intent = this.getIntent();
		String paperName = intent.getStringExtra("paperName");
		List<Question> questionList = new ArrayList<Question>();
		final MyDbHelper dbHelper = new MyDbHelper(getApplicationContext());
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor
				cursor = db.query("questions",
				new String[]{"id", "context", "answer", "right_answer", "type"}, "paper=?",
				new String[]{paperName}, "", "", "id desc");
		Question tempQuestion;
		while (cursor.moveToNext()) {
			tempQuestion = new Question();
			tempQuestion.setId(cursor.getInt(0));
			tempQuestion.setContext(cursor.getString(1));
			tempQuestion.setAnswer(cursor.getString(2));
			tempQuestion.setRightAnswer(cursor.getString(3));
			String type = cursor.getString(4);
			if (type.equals("CHOICE_QUESTION")) {
				tempQuestion.setType(QuestionTypes.CHOICE_QUESTION);
			} else if (type.equals("JUDGE_QUESTION")) {
				tempQuestion.setType(QuestionTypes.JUDGE_QUESTION);
			} else {
				tempQuestion.setType(QuestionTypes.FILLING_QUESTION);
			}
			questionList.add(tempQuestion);
		}
		cursor.close();

		if (questionList.size() > 0) {
			tempQuestion = questionList.get(0);
			this.type = tempQuestion.getType().toString();
			this.rightAnswer = tempQuestion.getRightAnswer();
			this.answerList = tempQuestion.getAnswer();
			TextView contextTextView = (TextView)findViewById(R.id.reviewQuestionContextTextView);
			contextTextView.setText(tempQuestion.getId() + ". " + tempQuestion.getContext());
			LinearLayout answerLinearLayout = (LinearLayout)findViewById(R.id.reviewAnswerLinearLayout);
			if(tempQuestion.getType().compareTo(QuestionTypes.CHOICE_QUESTION) == 0 ||
					tempQuestion.getType().compareTo(QuestionTypes.JUDGE_QUESTION) == 0) {
				List<String> answerList = tempQuestion.getAnswer();
				RadioGroup radioGroup = new RadioGroup(answerLinearLayout.getContext());
				RadioButton radioButton;
				for(String s : answerList) {
					radioButton = new RadioButton(radioGroup.getContext());
					radioButton.setText(s);
					radioGroup.addView(radioButton);
				}

				radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					public void onCheckedChanged(RadioGroup group,int id) {
						checkedId = id;
					}
				});
				answerLinearLayout.addView(radioGroup);

			} else {
				TextView answerTextView = new TextView(getApplicationContext());
				this.answerViewId = View.generateViewId();
				answerTextView.setId(this.answerViewId);
				answerLinearLayout.addView(answerTextView);
			}
		} else {
			Toast.makeText(getApplicationContext(), "此试卷无题目", Toast.LENGTH_SHORT).show();
			this.finish();
		}

		Button okButton = (Button) findViewById(R.id.reviewOkButton);
		okButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(type.equals("CHOICE_QUESTION") || type.equals("JUDGE_QUESTION")) {
					RadioButton radio = (RadioButton)findViewById(checkedId);
					if(radio != null && radio.getText().equals(rightAnswer)) {
						Toast.makeText(getApplicationContext(), "恭喜你答对了", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(), "答错了", Toast.LENGTH_SHORT).show();
					}
				} else {
					if(((TextView)findViewById(answerViewId)).getText().equals(rightAnswer)) {
						Toast.makeText(getApplicationContext(), "恭喜你答对了", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(getApplicationContext(), "答错了", Toast.LENGTH_SHORT).show();
					}
				}
				ReviewActivity.this.finish();
			}
		});
	}
}

package me.zhht1204.reviewyourself.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import me.zhht1204.reviewyourself.R;
import me.zhht1204.reviewyourself.utils.MyDbHelper;

public class MainActivity extends AppCompatActivity {
    SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button addButton = (Button)findViewById(R.id.addQuestionButton);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), EditQuestionActivity.class);
                startActivity(intent);
            }
        });

	    Button startButton = (Button)findViewById(R.id.startReviewButton);
	    startButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    Intent intent = new Intent();
			    intent.putExtra("type", "review");
			    intent.setClass(getApplicationContext(), PaperListActivity.class);
			    startActivity(intent);
		    }
	    });

	    Button editButton = (Button)findViewById(R.id.editPaperButton);
	    editButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    Intent intent = new Intent();
			    intent.putExtra("type", "edit");
			    intent.setClass(getApplicationContext(), PaperListActivity.class);
			    startActivity(intent);
		    }
	    });

        Button exitButton = (Button)findViewById(R.id.exitButton);
	    exitButton.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View view) {
			    MainActivity.this.finish();
		    }
	    });
    }
}

package me.zhht1204.reviewyourself.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutCompat;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import me.zhht1204.reviewyourself.R;
import me.zhht1204.reviewyourself.utils.MyDbHelper;

public class EditQuestionActivity extends AppCompatActivity {
	private int questionTypeId;
	private int paperId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_question);
		List<String> questionTypeList = new ArrayList<>();
		questionTypeList.add("选择题");
		questionTypeList.add("判断题");
		questionTypeList.add("填空题");
		ArrayAdapter<String> adapter =
				new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,
						questionTypeList);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner questionTypeSpinner = (Spinner) findViewById(R.id.questionTypeSpinner);
		questionTypeSpinner.setAdapter(adapter);
		questionTypeSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				questionTypeId = i;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		List<String> paperList = new ArrayList<>();
		final MyDbHelper dbHelper = new MyDbHelper(getApplicationContext());
		SQLiteDatabase db = dbHelper.getReadableDatabase();
		Cursor cursor = db.query("papers", new String[]{"paper"}, "", null, "", "", "id desc");
		while (cursor.moveToNext()) {
			paperList.add(cursor.getString(0));
		}
		cursor.close();
		adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, paperList);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		Spinner paperSpinner = (Spinner) findViewById(R.id.paperSpinner);
		paperSpinner.setAdapter(adapter);
		paperSpinner.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
				EditQuestionActivity.this.paperId = i;
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
			}
		});

		final String paper = paperList.get(this.paperId);
		Button editOkButton = (Button) findViewById(R.id.editOkButton);
		editOkButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				String context =
						(((EditText) (findViewById(R.id.questionContextEditText)))).getText()
								.toString();
				if (context.isEmpty()) {
					Toast.makeText(getApplicationContext(), "请填写题目", Toast.LENGTH_SHORT).show();
					return;
				}
				String type;
				if (EditQuestionActivity.this.questionTypeId == 0) {
					type = "CHOICE_QUESTION";
				} else if (EditQuestionActivity.this.questionTypeId == 1) {
					type = "JUDGE_QUESTION";
				} else {
					type = "FILLING_QUESTION";
				}
				String answer =
						(((EditText) (findViewById(R.id.questionAnswerEditText)))).getText()
								.toString().trim().replace('\n', ';');
				String rightAnswer = (answer.length() > 0 && answer.indexOf("【") > -1) ?
						answer.substring(answer.indexOf("【") + 1, answer.indexOf("】")) : answer;
				answer = answer.replace("【", "").replace("】", "");
				Intent intent = getIntent();
				Bundle bundle = intent.getBundleExtra("question");
				String sql;
				if (bundle != null) {
					int id = bundle.getInt("id");
					sql = "update questions set "
							+ "context = '" + context + "', "
							+ "answer = '" + answer + "', "
							+ "right_answer = '" + rightAnswer + "', "
							+ "type = '" + type + "', "
							+ "paper = '" + paper + "' "
							+ "where id = " + id + ";";
				} else {
					sql = "insert into questions(context, answer, right_answer, type, paper) "
							+ "values("
							+ "'" + context + "', "
							+ "'" + answer + "', "
							+ "'" + rightAnswer + "', "
							+ "'" + type + "', "
							+ "'" + paper + "');";
				}
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				db.execSQL(sql);
				Toast.makeText(getApplicationContext(), "保存成功", Toast.LENGTH_SHORT).show();
				EditQuestionActivity.this.finish();
			}
		});
	}
}

package me.zhht1204.reviewyourself.objects;

import java.util.ArrayList;
import java.util.List;

public class Question {
	private int id;
    private QuestionTypes type;
    private String context;
    private List<String> answer;
	private String rightAnswer;
	private String paper;

    public Question() {
    }
    public Question(QuestionTypes type) {
        this.type = type;
        if(type.compareTo(QuestionTypes.CHOICE_QUESTION) == 0) {
            this.answer = new ArrayList<String>(4);
        } else if(type.compareTo(QuestionTypes.JUDGE_QUESTION) == 0 ||
                type.compareTo(QuestionTypes.FILLING_QUESTION) == 0
                ) {
            this.answer = new ArrayList<>(1);
        }
    }
    public Question(QuestionTypes type, String context, List<String> answer, String rightAnswer) {
        this.type = type;
        this.context = context;
        this.answer = answer;
	    this.rightAnswer = rightAnswer;
    }

    public int getAnswerNumbers() {
        return this.answer.size();
    }

    public String getContext() {
        return context;
    }
    public void setContext(String context) {
        this.context = context;
    }

    public QuestionTypes getType() {
        return type;
    }
    public void setType(QuestionTypes type) {
        this.type = type;
    }

    public List<String> getAnswer() {
        return answer;
    }
    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }
	public void setAnswer(String answer) {
		String[] result = answer.split(";");
		List<String> resultList = new ArrayList<String>();
		for(String s : result) {
			resultList.add(s);
		}
		this.answer = resultList;
	}

	public String getRightAnswer() {
		return rightAnswer;
	}
	public void setRightAnswer(String rightAnswer) {
		this.rightAnswer = rightAnswer;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getPaper() {
		return paper;
	}
	public void setPaper(String paper) {
		this.paper = paper;
	}
}

package me.zhht1204.reviewyourself.objects;

public enum QuestionTypes {
    CHOICE_QUESTION, JUDGE_QUESTION, FILLING_QUESTION;
}
